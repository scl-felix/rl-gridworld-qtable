module Env.Types exposing (..)

import Dict exposing (..)


type Grid
    = NoGrid
    | Grid Extent (List Position) Cells


type alias Extent =
    ( Int, Int )


type alias Cells =
    Dict Position Cell


type alias Position =
    ( Int, Int )



{- NoStae and Home are terminal states -}


type State
    = NoState
    | Start
    | Safe
    | Food
    | Poison
    | Home


type alias Cell =
    { pos : Position
    , state : State
    , reward : Float
    }


type Action
    = Up
    | Down
    | Left
    | Right


actionName : Action -> String
actionName act =
    case
        act
    of
        Up ->
            "Up"

        Down ->
            "Down"

        Left ->
            "Left"

        Right ->
            "Right"
