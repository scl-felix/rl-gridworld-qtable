module Env.Gridworld exposing (..)

import Dict exposing (..)
import Env.Types exposing (..)
import List.Extra exposing (..)


default : Extent -> Grid
default ( rows, cols ) =
    Grid
        ( rows, cols )
        [ ( 1, 1 ) ]
        (Dict.empty
            |> update ( 1, 1 ) Start 20.0
            |> update ( 1, 2 ) Safe -1.0
            |> update ( 1, 3 ) Safe -1.0
            |> update ( 2, 1 ) Safe -1.0
            |> update ( 2, 2 ) Safe -1.0
            |> update ( 2, 3 ) Safe -1.0
            |> update ( 3, 1 ) Safe -1.0
            |> update ( 3, 2 ) Safe -1.0
            |> update ( rows, cols ) Home 10.0
        )



{- make a list of all possible (row, col) Positions -}


genPositions : Int -> Int -> List Position
genPositions rows cols =
    List.range 1 rows
        |> List.concatMap (addCols cols)


addCols : Int -> Int -> List Position
addCols cols row =
    List.range 1 cols
        |> List.map (addPos row)


addPos : Int -> Int -> Position
addPos row col =
    ( row, col )



{- set cells to a state and reward -}


update : Position -> State -> Float -> Cells -> Cells
update pos state reward cells =
    Dict.insert pos (Cell pos state reward) cells



{- set cells to specific state with default reward -}


addPoisonCell : Position -> Cells -> Cells
addPoisonCell pos cells =
    update pos Poison -10.0 cells


addFoodCell : Position -> Cells -> Cells
addFoodCell pos cells =
    update pos Food 1.0 cells



{- create safe cells with default reward on a grid of given Extent
   ALSO
   at default positions adds Start, Poison, Food and Home cells
-}


init : Extent -> Grid
init ( rows, cols ) =
    let
        cells =
            Dict.empty
    in
    genPositions rows cols
        |> List.foldr (\pos cells -> update pos Safe -1.0 cells) cells
        |> update ( 1, 1 ) Start 20.0
        |> update ( rows, cols ) Home 10.0
        |> addPoisonCell ( 3, 3 )
        |> addFoodCell ( 4, 4 )
        |> Grid ( rows, cols ) [ ( 1, 1 ) ]



{- ==================================================
   Helpers - bit twisted, do away with NoGrid, use more maybe's
   ==================================================
-}
{- maybe allow user-defined terminal states -}


isTerminal : State -> Bool
isTerminal state =
    let
        _ =
            Debug.log "isterminal called with state " state
    in
    state == Home



--    (state == Home) || (state == Poison)


cell : Grid -> Cell
cell grid =
    case
        grid
    of
        NoGrid ->
            Cell ( 0, 0 ) NoState 0.0

        Grid extent (pos :: positions) cells ->
            Dict.get pos cells
                |> Maybe.withDefault (Cell ( 0, 0 ) NoState 0.0)
                |> Debug.log "Env.Gridworld.cell with this grid "

        _ ->
            Debug.crash "Arrrrggggghhhhhhhhhhhhhhhhh"


reward : Grid -> Float
reward grid =
    cell grid
        |> .reward


position : Grid -> Position
position grid =
    cell grid
        |> .pos


isInTerminalState : Grid -> Bool
isInTerminalState grid =
    cell grid
        |> Debug.log "isInTerminalState: The cell is: "
        |> .state
        |> isTerminal


rewardAtPosition : Grid -> Position -> Float
rewardAtPosition grid pos =
    case
        grid
    of
        NoGrid ->
            0.0

        Grid extent positions cells ->
            let
                cell =
                    Dict.get pos cells
            in
            Maybe.map .reward cell
                |> Maybe.withDefault 0.0


path : Grid -> List Position
path grid =
    case
        grid
    of
        NoGrid ->
            []

        Grid _ positions _ ->
            positions



-- maybe do this later
--health : Grid -> Float
--health grid =
--    case
--        grid
--    of
--        NoGrid ->
--            0.0
--        Grid extent path cells ->
--            List.foldl (\pos health -> ()) path
--            |> .reward
{- ==================================================
   Actions => Movement
   ==================================================
-}


move : Grid -> Action -> Grid
move grid action =
    case
        grid
    of
        NoGrid ->
            NoGrid

        Grid extent (pos :: rest) cells ->
            let
                newPos =
                    newPosition pos action extent
            in
            Grid extent (newPos :: (pos :: rest)) cells

        _ ->
            Debug.crash "Whoa - this cannot be true!"


newPosition : Position -> Action -> Extent -> Position
newPosition pos action extent =
    case
        action
    of
        Up ->
            maybeMoveBy ( -1, 0 ) pos extent

        Down ->
            maybeMoveBy ( 1, 0 ) pos extent

        Left ->
            maybeMoveBy ( 0, -1 ) pos extent

        Right ->
            maybeMoveBy ( 0, 1 ) pos extent


maybeMoveBy : Extent -> Position -> Extent -> Position
maybeMoveBy ( rowMove, colMove ) ( row, col ) extent =
    let
        newPos =
            ( row + rowMove, col + colMove )
    in
    if validPosition newPos extent then
        newPos
    else
        ( row, col )


validPosition : Position -> Extent -> Bool
validPosition ( row, col ) ( rows, cols ) =
    (row >= 1) && (row <= rows) && (col >= 1) && (col <= cols)
