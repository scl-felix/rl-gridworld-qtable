module Env.View exposing (..)

import Dict exposing (..)
import Env.Gridworld as Gridworld exposing (..)
import Env.Types exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick)
import Svg exposing (..)
import Svg.Attributes exposing (..)
import Svg.Events exposing (..)


squareSize =
    50


scaleUp : Int -> Int
scaleUp x =
    (x - 1)
        * squareSize
        + (squareSize // 2)


view : Grid -> List (Html msg)
view grid =
    renderGrid grid
        ++ [ renderPath (Gridworld.path grid)
           , renderPosition (Gridworld.position grid)
           ]



{- =============================================================
   Rendering the grid of squares
   =============================================================
-}


renderGrid : Grid -> List (Html msg)
renderGrid grid =
    case grid of
        NoGrid ->
            []

        Grid ( rows, cols ) path cells ->
            let
                w =
                    toString ((rows + 1) * 100)

                h =
                    toString ((cols + 1) * 100)

                dimensions =
                    "0, 0 " ++ w ++ ", " ++ h
            in
            renderCells ( rows, cols ) cells


renderCells : Extent -> Cells -> List (Html msg)
renderCells ( rows, cols ) cells =
    List.range 1 rows
        |> List.foldl (renderCols cells cols) []


renderCols : Cells -> Int -> Int -> List (Html msg) -> List (Html msg)
renderCols cells cols row squares =
    List.range 1 cols
        |> List.foldl (addSquare cells row) squares


addSquare : Cells -> Int -> Int -> List (Html msg) -> List (Html msg)
addSquare cells row col squares =
    renderSquare cells ( row, col ) squares


renderSquare : Cells -> Position -> List (Html msg) -> List (Html msg)
renderSquare cells ( row, col ) squares =
    let
        colour =
            lookupCellColour cells ( row, col )

        --            "rgba(127, 127, 127, 100)"
        xpos =
            toString ((col - 1) * squareSize)

        ypos =
            toString ((row - 1) * squareSize)
    in
    rect
        [ fill colour
        , x xpos
        , y ypos
        , Svg.Attributes.width (toString squareSize)
        , Svg.Attributes.height (toString squareSize)
        ]
        []
        :: squares


lookupCellColour : Cells -> Position -> String
lookupCellColour cells pos =
    Maybe.map cellColour (Dict.get pos cells)
        |> Maybe.withDefault "rgba(200, 200, 200, 100)"


cellColour : Cell -> String
cellColour cell =
    case cell.state of
        Start ->
            "rgba(0, 100, 0, 100)"

        Safe ->
            "rgba(100, 100, 100, 100)"

        Poison ->
            "rgba(10, 10, 10, 100)"

        Food ->
            "rgba(100, 0, 0, 100)"

        Home ->
            "rgba(0, 0, 100, 100)"

        NoState ->
            "rgba(0, 0, 0, 100)"



{- =============================================================
   Rendering the path taken
   =============================================================
-}


renderPath : List Position -> Html msg
renderPath positions =
    let
        interjected =
            positions
                |> List.foldl toPathString ""
    in
    polyline [ fill "none", stroke "blue", points interjected ] []


toPathString : Position -> String -> String
toPathString ( row, col ) points =
    points ++ toString (scaleUp col) ++ "," ++ toString (scaleUp row) ++ " "


renderPosition : Position -> Html msg
renderPosition ( row, col ) =
    let
        colour =
            "rgba(0, 0, 0, 100)"
    in
    circle
        [ fill colour
        , cx (toString (scaleUp col))
        , cy (toString (scaleUp row))
        , r (toString 10)
        ]
        []
