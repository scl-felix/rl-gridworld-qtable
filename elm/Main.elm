module Main exposing (..)

import Html exposing (Html)
import Model exposing (init, subscriptions)
import Update exposing (update)
import View exposing (view)


main =
    Html.program
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }
