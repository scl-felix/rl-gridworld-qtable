module View exposing (view)

import Env.Gridworld exposing (..)
import Env.Types exposing (..)
import Env.View exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Model exposing (..)
import Policy.View exposing (..)
import Svg exposing (..)
import Svg.Attributes exposing (..)
import Svg.Events exposing (..)


view : Model -> Html Msg
view model =
    let
        w =
            toString ((model.cols + 1) * 100)

        h =
            toString ((model.rows + 1) * 100)

        dimensions =
            "0, 0 " ++ w ++ ", " ++ h
    in
    div []
        [ input [ placeholder "Rows", Html.Events.onInput SetRows ] []
        , input [ placeholder "Cols", Html.Events.onInput SetCols ] []
        , button [ Html.Events.onClick StartPlay ] [ Html.text "Start!" ]
        , button [ Html.Events.onClick StopPlay ] [ Html.text "Stop" ]
        , button [ Html.Events.onClick (Move Up) ] [ Html.text "Up" ]
        , button [ Html.Events.onClick (Move Down) ] [ Html.text "Down" ]
        , button [ Html.Events.onClick (Move Left) ] [ Html.text "Left" ]
        , button [ Html.Events.onClick (Move Right) ] [ Html.text "Right" ]
        , br [] []
        , div
            []
            [ Html.text ("Reward = " ++ toString model.reward)
            , Html.text ("Health = " ++ toString model.health)
            ]
        , Policy.View.view model.policy
        , svg [ version "1.1", x "0", y "0", viewBox dimensions ] (render model)
        ]


render : Model -> List (Html Msg)
render model =
    Env.View.view model.grid
