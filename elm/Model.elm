module Model exposing (..)

import AnimationFrame exposing (..)
import Dict exposing (Dict)
import Env.Gridworld exposing (..)
import Env.Types exposing (..)
import Html exposing (..)
import Json.Decode exposing (..)
import Keyboard exposing (..)
import Policy.Policy exposing (..)
import Time exposing (..)


-- Model is the Environment!


type alias Model =
    { running : Bool
    , rows : Int
    , cols : Int

    -- env
    , grid : Grid
    , policy : Policy
    , reward : Float
    , health : Float
    , results : List Float
    }


type Msg
    = Cycle Time
    | StartPlay
    | StopPlay
    | SetRows String
    | SetCols String
    | AddPoison String
    | AddFood String
    | Move Action
    | KeyDown Int


model : Model
model =
    Model False 0 0 NoGrid Policy.Policy.default 0.0 0.0 []



--    Model False 0 0 NoGrid ( 1, 1 ) [] 0.0 0.0


init : ( Model, Cmd Msg )
init =
    ( model, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions model =
    if model.running then
        Keyboard.downs KeyDown
        --        Sub.batch
        --        [ AnimationFrame.diffs Cycle
        --        , Keyboard.downs KeyDown
        --        ]
    else
        Sub.none
