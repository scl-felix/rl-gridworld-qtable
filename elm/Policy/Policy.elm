module Policy.Policy exposing (..)

import Dict exposing (..)
import Env.Types exposing (..)
import List.Extra exposing (..)


type Policy
    = Policy Hyper (List Action) QTable


type alias Hyper =
    { learningRate : Float
    , discountRate : Float
    , eGreedy : Float
    }


type alias QTable =
    Dict Position (List ( Action, Float ))


default : Policy
default =
    Policy
        { learningRate = 1.0
        , discountRate = 1.0
        , eGreedy = 0.1
        }
        [ Up, Down, Left, Right ]
        Dict.empty


defaultActions : List Action -> List ( Action, Float )
defaultActions actions =
    actions
        |> List.map (\act -> ( act, 1.0 ))



{-
      first tho' - quite tricky creating the table entries dynamically,
      gonna go for it assuming QTable is fully initialised with every Position
      having a list of (Action, Float)


       Ah - could keep Qtable immutabl here, use deafault actions if none
       in qtable and let observer do the create/update

   later...
       hmm - try assuming position (0,0) is the terminal state

-}


select : Policy -> Position -> ( Action, Float )
select policy pos =
    case policy of
        Policy hyper actions qTable ->
            --List (Action, Float)
            Dict.get pos qTable
                |> Maybe.withDefault (defaultActions actions)
                |> bestAction


bestAction : List ( Action, Float ) -> ( Action, Float )
bestAction acts =
    let
        default =
            ( Up, 0.0 )

        -- gotta be a better way !
    in
    List.foldl
        (\( act, rew ) ( bestAct, bestRew ) ->
            if rew > bestRew then
                ( act, rew )
            else
                ( bestAct, bestRew )
        )
        default
        acts



{- update policy for [pos, act] with new discounted reward

   ALSO has to add to qtable any states (Position => List (Action, Float))
        because we don't need to initialise it and even if we did, there
        would STILL be maybes to deal with
-}


observe : Policy -> Position -> Action -> Float -> Position -> Policy
observe policy pos act reward newPos =
    case policy of
        Policy hyper actions qTable ->
            let
                ( nextAct, nextReward ) =
                    select policy newPos

                discountedFutureReward =
                    hyper.discountRate * nextReward

                newReward =
                    reward + hyper.learningRate * (reward + discountedFutureReward)

                dummy =
                    Debug.log "observe called with pos " pos

                newQTable =
                    qInsert pos act newReward actions qTable
            in
            Policy hyper actions newQTable



-- gotta add it to the list of (Action, Floats) in dictionary at pos


qInsert : Position -> Action -> Float -> List Action -> QTable -> QTable
qInsert pos act reward actions qTable =
    let
        acts =
            Dict.get pos qTable
                |> Maybe.withDefault (defaultActions actions)
                |> List.map
                    (\( a, r ) ->
                        if a == act then
                            ( a, reward )
                        else
                            ( a, r )
                    )

        dummy =
            Debug.log "qInsert called with pos " pos

        got =
            Debug.log "Dict.get from pos gives us action - reward list " (Dict.get pos qTable)
    in
    Dict.insert pos acts qTable
