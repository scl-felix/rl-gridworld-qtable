module Policy.View exposing (..)

import Dict exposing (..)
import Env.Types exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick)
import Policy.Policy exposing (..)


view : Policy -> Html msg
view policy =
    case
        policy
    of
        Policy hyper actions qTable ->
            renderQTable qTable


renderQTable : QTable -> Html msg
renderQTable qTable =
    let
        rows =
            Dict.foldl (\pos acts rows -> addTableRow pos acts rows) [ div [] [] ] qTable
    in
    div
        []
        [ table
            []
            rows
        ]



--addTableRow : Position -> (List (Action, Float)) -> List (Html msg) -> List (Html msg)


addTableRow ( row, col ) acts html =
    html
        ++ [ tr
                []
                (text (toString row ++ ", " ++ toString col)
                    :: List.map (\( act, reward ) -> addTableCell act reward) acts
                )
           ]


addTableCell : Action -> Float -> Html msg
addTableCell act reward =
    td
        []
        [ text (actionName act ++ " -> " ++ toString reward) ]
