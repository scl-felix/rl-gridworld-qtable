module Update exposing (..)

import Env.Gridworld exposing (..)
import Env.Types exposing (..)
import Model exposing (..)
import Policy.Policy exposing (..)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        SetRows rows ->
            ( { model | rows = easyInt rows }, Cmd.none )

        SetCols cols ->
            ( { model | cols = easyInt cols }, Cmd.none )

        AddPoison reward ->
            ( model, Cmd.none )

        AddFood reward ->
            ( model, Cmd.none )

        StartPlay ->
            ( { model
                | running = True
                , grid = Env.Gridworld.init ( model.rows, model.cols )
              }
            , Cmd.none
            )

        StopPlay ->
            ( { model
                | running = False
              }
            , Cmd.none
            )

        Cycle diff ->
            -- for a self-play QTable Policy
            let
                grid =
                    model.grid

                policy =
                    model.policy

                ( action, reward ) =
                    Policy.Policy.select policy (position grid)
            in
            ( move model action, Cmd.none )

        KeyDown chCode ->
            -- from the keyboard - for manual play/learn Policy
            -- going to observe these moves - just to see the QTable growing
            let
                newModel =
                    move model (actionOfKey chCode)
            in
            if isInTerminalState newModel.grid then
                ( { newModel
                    | running = False
                    , results = model.health :: model.results
                  }
                , Cmd.none
                )
            else
                ( newModel, Cmd.none )

        Move action ->
            -- from the buttons, mocking movement
            let
                newModel =
                    move model action
            in
            if isInTerminalState newModel.grid then
                ( { newModel
                    | running = False
                    , results = model.health :: model.results
                  }
                , Cmd.none
                )
            else
                ( newModel, Cmd.none )


move : Model -> Action -> Model
move model action =
    let
        newGrid =
            Env.Gridworld.move model.grid action

        newReward =
            Env.Gridworld.reward newGrid

        newPosition =
            Env.Gridworld.position newGrid

        newHealth =
            model.health + newReward

        newPolicy =
            Policy.Policy.observe
                model.policy
                (Env.Gridworld.position model.grid)
                action
                newReward
                newPosition
    in
    { model
        | grid = newGrid
        , reward = newReward
        , health = newHealth
        , policy = newPolicy
    }


actionOfKey : Int -> Action
actionOfKey ch =
    case
        ch
    of
        65 ->
            --a
            Left

        83 ->
            --s
            Right

        68 ->
            --d
            Up

        70 ->
            --f
            Down

        _ ->
            --any other key
            Up


easyInt : String -> Int
easyInt v =
    Result.withDefault 0 (String.toInt v)
